package week8;

public class TestShape{
    public static void main(String[] args) throws Exception{
        Rectangle rect1 = new Rectangle("rect1",10,5);
        rect1.PrintAreaRect();
        rect1.PrintPerimeterRect();
        System.out.println();

        Rectangle rect2 = new Rectangle("rect2",5,3);
        rect2.PrintAreaRect();
        rect2.PrintPerimeterRect();
        System.out.println();
;
        Circle circle1 = new Circle("circle1",1);
        circle1.PrintAreaCircle();
        circle1.PrintPerimeterCircle();
        System.out.println();

        Circle circle2 = new Circle("circle2",2);
        circle2.PrintAreaCircle();
        circle2.PrintPerimeterCircle();
        System.out.println();

        Triangle triangle1 = new Triangle("trianlge1",5,5,6);
        triangle1.PrintAreaTriangle();
        triangle1.PrintPerimeterTriangle();
        System.out.println();


    }
}