package week8;

public class Circle {
    private double r;
    private double area;
    private double perimeter;
    private String name;
    public Circle(String name ,int r){
        this.r = r;
        this.name = name;
    }
    public void PrintAreaCircle(){
        area = Math.PI*(Math.pow(r,2));
        System.out.println(name+" Area of Circle = " + area);
    }
    public void PrintPerimeterCircle(){
        perimeter = 2*(Math.PI*r);
        System.out.println(name+ " Perimeter of Circle = " + perimeter);

    }
}
