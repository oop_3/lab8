package week8;

public class Rectangle {
    private double width;
    private double height;
    private double area;
    private double perimeter; 
    private String name;
    public Rectangle(String name, double width, double height){
        this.width = width;
        this.height = height;
        this.name = name;
    }
    public void PrintAreaRect(){
        area = width * height;
        System.out.println(name+" Area  = "+area);
    }
    public void PrintPerimeterRect(){
        perimeter = (width *2)+ (height*2);
        System.out.println(name+" Perimeter  = "+perimeter);
    }
}
