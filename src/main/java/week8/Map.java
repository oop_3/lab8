package week8;

public class Map {
    private int width;
    private int height;
    public final static int X_MIN = 0;
    public final static int Y_MIN = 0;
    public Map(int width, int height) {
        this.width = width;
        this.height = height;
    }
    public void print() {
        for(int i = X_MIN; i < width; i++){
            for(int j = Y_MIN; j < height;j++){
                System.out.print( "-");
            }
        System.out.print("\n");
        }
    }

    
}
